import { type Plugin, compatibilityCheck } from "html-validate";
import configs from "./configs";
import rules from "./rules";

interface PackageJson {
	name: string;
	peerDependencies: Record<string, string>;
}

/* eslint-disable-next-line @typescript-eslint/no-require-imports -- want to import at runtime */
const pkg = require("../package.json") as PackageJson;

/* warn when using unsupported html-validate library version */
const range = pkg.peerDependencies["html-validate"];
compatibilityCheck(pkg.name, range);

const plugin: Plugin = {
	name: pkg.name,

	/* Add predefined configurations */
	configs,

	/* Add all rules from rules folder */
	rules,
};

export = plugin;
