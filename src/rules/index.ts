import ExampleRule from "./example";

const rules = {
	"example/rule": ExampleRule,
};

export default rules;
